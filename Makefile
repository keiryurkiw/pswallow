.POSIX:
.SUFFIXES:

PREFIX = /usr/local
PKG_CONFIG = pkg-config
#CC = gcc
#CFLAGS = -O2 -pipe
#CPPFLAGS = -DNDEBUG

XCB = `$(PKG_CONFIG) --cflags --libs xcb`

all: pswallow

pswallow: pswallow.c Makefile
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ $< $(LDFLAGS) $(XCB)

clean:
	rm -f pswallow

install: all
	mkdir -p '$(DESTDIR)$(PREFIX)/bin'
	cp -f pswallow '$(DESTDIR)$(PREFIX)/bin'
	chmod 755 '$(DESTDIR)$(PREFIX)/bin/pswallow'

uninstall:
	rm -f '$(DESTDIR)$(PREFIX)/bin/pswallow'

.PHONY: all clean install uninstall
