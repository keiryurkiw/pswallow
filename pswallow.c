/*
 * pswallow is a pseudo window swallower utilizing XCB.
 *
 * Copyright (C) 2023  Keir Yurkiw
 *
 * pswallow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pswallow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pswallow.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _POSIX_C_SOURCE 200809L
#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include <xcb/xcb.h>

static void
perr(char *fmt, ...)
{
	fputs("pswallow: error: ", stderr);

	va_list args;
	va_start(args, fmt);
	vfprintf(stderr, fmt, args);
	va_end(args);

	fputc('\n', stderr);
}

static xcb_window_t
get_focused_window(xcb_connection_t *conn)
{
	xcb_get_input_focus_cookie_t cookie = xcb_get_input_focus(conn);
	xcb_get_input_focus_reply_t *reply = xcb_get_input_focus_reply(
			conn, cookie, NULL);

	xcb_window_t win = reply->focus;
	free(reply);
	return win;
}

static int
run_command(char **argv)
{
	pid_t pid = fork();
	if (pid == 0) {
		execvp(argv[1], argv+1);

		/* When execvp() succeeds, this code is never ran. */
		perr("Failed to run command '%s': %s", argv[1], strerror(errno));
		exit(errno);
	} else if (pid > 0) {
		int status;
		wait(&status);
		return WEXITSTATUS(status);
	}

	perr("Failed to fork process: %s", strerror(errno));
	return errno;
}

static void
map_window(xcb_connection_t *conn, xcb_window_t window)
{
	xcb_map_window(conn, window);
	xcb_flush(conn);

	/*
	 * Wait for window to actually be shown before exiting, otherwise
	 * the window will remain unmapped.
	 */
	xcb_get_window_attributes_cookie_t cookie;
	xcb_get_window_attributes_reply_t *reply;
	while (1) {
		cookie = xcb_get_window_attributes(conn, window);
		reply = xcb_get_window_attributes_reply(conn, cookie, NULL);

		xcb_map_state_t map_state = reply->map_state;
		free(reply);
		if (map_state == XCB_MAP_STATE_VIEWABLE)
			return;

		nanosleep(&(struct timespec){ .tv_nsec = 500000 /* 0.5ms */ }, NULL);
	}
}

int
main(int argc, char *argv[])
{
	if (argc <= 1) {
		fputs("usage: pswallow [program] [args]\n", stderr);
		return 1;
	}

	xcb_connection_t *conn = xcb_connect(NULL, NULL);
	if (xcb_connection_has_error(conn)) {
		perr("Failed to connect to the X server");
		xcb_disconnect(conn);
		return 1;
	}

	xcb_window_t window = get_focused_window(conn);

	xcb_unmap_window(conn, window);
	xcb_flush(conn);

	int ret = run_command(argv);

	map_window(conn, window);

	xcb_disconnect(conn);
	return ret;
}
