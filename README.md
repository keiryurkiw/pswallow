# pswallow: A pseudo window swallower for X11

pswallow (pronounced swallow) is a pseudo window swallower written entirely
in C99 utilizing the XCB library. It works by finding the currently visible
window (the terminal window) and hiding it until the specified command
has finished.

## Dependencies

- libxcb

## Installation

```sh
make && sudo make install
```

## Uninstallation

```sh
sudo make uninstall
```
